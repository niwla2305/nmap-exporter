# nmap Prometheus Exporter
small utility program to export a network scan from nmap to prometheus

## Bare Metal Usage
* `copy .env.example to .env and adjust it where neccessary`
* `pip3 install -r requirements.txt`
* `python3 main.py`
Service is running on port 8000

## Docker use
* `copy .env.example to .env and adjust it where neccessary`
* build the image: `sudo docker build . -t nmap-exporter`
* use it in your prometheus stack. Exposed port is 33834