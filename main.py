from prometheus_client import start_http_server
from prometheus_client.core import GaugeMetricFamily, REGISTRY
import time
import subprocess
import os
from xml.etree import ElementTree
import dotenv

dotenv.load_dotenv()


class NmapCollector(object):
    @staticmethod
    def collect():
        print("running network ping scan")
        g = GaugeMetricFamily(
            'network_device_ping_time_ms',
            'Ping times of all network devices (devices are labels)',
            labels=["hostname", "ip_address"]
        )

        filename = f"/tmp/nmap-exporter-output-{time.time()}"

        subprocess.Popen(
            ["nmap", "-oX", filename, "-sn", os.environ["IP_RANGE"]],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL
        ).wait()

        root_node = ElementTree.parse(filename).getroot()
        for node in root_node.findall("host"):
            address = node.find("address").attrib["addr"]
            ping_time = node.find("times").attrib["srtt"]
            hostnames_el = node.find("hostnames").find("hostname")
            if hostnames_el is not None:
                hostname = hostnames_el.attrib["name"]
            else:
                hostname = address

            g.add_metric([hostname, address], int(ping_time) / 1000)

        yield g
        os.remove(filename)
        print("scan completed")


REGISTRY.register(NmapCollector)

if __name__ == '__main__':
    start_http_server(int(os.environ["PORT"]))
    while True:
        time.sleep(1000)
